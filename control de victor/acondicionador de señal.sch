<Qucs Schematic 0.0.15>
<Properties>
  <View=0,1,844,740,1,0,0>
  <Grid=10,10,1>
  <DataSet=acondicionador de señal.dat>
  <DataDisplay=acondicionador de señal.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=T\x00EDtulo>
  <FrameText1=Dibujado por:>
  <FrameText2=Fecha:>
  <FrameText3=Revisi\x00F3n:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Vdc V1 1 320 370 -63 -26 0 3 "6 V" 1>
  <GND * 1 320 90 0 0 0 2>
  <Vdc V2 1 320 150 -63 -26 0 3 "6 V" 1>
  <Lib OP1 1 310 260 25 20 0 0 "OpAmps" 0 "uA741" 0>
  <R R1 1 430 260 -26 15 0 0 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <.TR TR1 1 710 30 0 71 0 0 "lin" 1 "0" 1 "5 ms" 1 "100" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <R R2 1 210 240 -26 -59 1 0 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <Vrect V6 1 120 290 18 -26 0 1 "5 V" 1 "1 ms" 1 "1 ms" 1 "1 ns" 0 "1 ns" 0 "0 ns" 0>
  <GND * 1 120 360 0 0 0 0>
  <GND * 1 320 490 0 0 0 0>
  <R R4 1 230 460 -26 -59 0 2 "1 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R3 1 120 460 -26 -59 0 2 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <GND * 1 500 320 0 0 0 0>
</Components>
<Wires>
  <320 90 320 120 "" 0 0 0 "">
  <320 300 320 340 "" 0 0 0 "">
  <320 180 320 200 "" 0 0 0 "">
  <370 260 400 260 "V_O" 390 200 11 "">
  <240 240 270 240 "V_I" 270 180 10 "">
  <120 240 180 240 "" 0 0 0 "">
  <120 240 120 260 "" 0 0 0 "">
  <120 320 120 360 "" 0 0 0 "">
  <320 400 320 460 "" 0 0 0 "">
  <320 460 320 490 "" 0 0 0 "">
  <260 460 320 460 "" 0 0 0 "">
  <60 460 90 460 "" 0 0 0 "">
  <60 200 60 460 "" 0 0 0 "">
  <320 200 320 220 "" 0 0 0 "">
  <60 200 320 200 "" 0 0 0 "">
  <150 460 180 460 "" 0 0 0 "">
  <220 280 270 280 "V_Com" 240 310 21 "">
  <220 280 220 370 "" 0 0 0 "">
  <180 370 220 370 "" 0 0 0 "">
  <180 460 200 460 "" 0 0 0 "">
  <180 370 180 460 "" 0 0 0 "">
  <460 260 500 260 "" 0 0 0 "">
  <500 260 500 320 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
