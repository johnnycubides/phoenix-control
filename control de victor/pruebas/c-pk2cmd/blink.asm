;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.5.5 #9386 (Linux)
; This file was generated Mon Nov  9 00:08:58 2015
;--------------------------------------------------------
; PIC port for the 14-bit core
;--------------------------------------------------------
;	.file	"blink.c"
	list	p=16f628a
	radix dec
	include "p16f628a.inc"
;--------------------------------------------------------
; config word(s)
;--------------------------------------------------------
	__config 0x2118
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	extern	_STATUSbits
	extern	_PORTAbits
	extern	_PORTBbits
	extern	_INTCONbits
	extern	_PIR1bits
	extern	_T1CONbits
	extern	_T2CONbits
	extern	_CCP1CONbits
	extern	_RCSTAbits
	extern	_CMCONbits
	extern	_OPTION_REGbits
	extern	_TRISAbits
	extern	_TRISBbits
	extern	_PIE1bits
	extern	_PCONbits
	extern	_TXSTAbits
	extern	_EECON1bits
	extern	_VRCONbits
	extern	_INDF
	extern	_TMR0
	extern	_PCL
	extern	_STATUS
	extern	_FSR
	extern	_PORTA
	extern	_PORTB
	extern	_PCLATH
	extern	_INTCON
	extern	_PIR1
	extern	_TMR1
	extern	_TMR1L
	extern	_TMR1H
	extern	_T1CON
	extern	_TMR2
	extern	_T2CON
	extern	_CCPR1
	extern	_CCPR1L
	extern	_CCPR1H
	extern	_CCP1CON
	extern	_RCSTA
	extern	_TXREG
	extern	_RCREG
	extern	_CMCON
	extern	_OPTION_REG
	extern	_TRISA
	extern	_TRISB
	extern	_PIE1
	extern	_PCON
	extern	_PR2
	extern	_TXSTA
	extern	_SPBRG
	extern	_EEDATA
	extern	_EEADR
	extern	_EECON1
	extern	_EECON2
	extern	_VRCON
	extern	__sdcc_gsinit_startup
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	global	_delay
	global	_main

	global PSAVE
	global SSAVE
	global WSAVE
	global STK12
	global STK11
	global STK10
	global STK09
	global STK08
	global STK07
	global STK06
	global STK05
	global STK04
	global STK03
	global STK02
	global STK01
	global STK00

sharebank udata_ovr 0x0070
PSAVE	res 1
SSAVE	res 1
WSAVE	res 1
STK12	res 1
STK11	res 1
STK10	res 1
STK09	res 1
STK08	res 1
STK07	res 1
STK06	res 1
STK05	res 1
STK04	res 1
STK03	res 1
STK02	res 1
STK01	res 1
STK00	res 1

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
UDL_blink_0	udata
r0x1001	res	1
r0x1000	res	1
r0x1002	res	1
r0x1003	res	1
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; reset vector 
;--------------------------------------------------------
STARTUP	code 0x0000
	nop
	pagesel __sdcc_gsinit_startup
	goto	__sdcc_gsinit_startup
;--------------------------------------------------------
; code
;--------------------------------------------------------
code_blink	code
;***
;  pBlock Stats: dbName = M
;***
;entry:  _main	;Function start
; 2 exit points
;has an exit
;functions called:
;   _delay
;   _delay
;   _delay
;   _delay
;1 compiler assigned register :
;   STK00
;; Starting pCode block
_main	;Function start
; 2 exit points
;	.line	13; "blink.c"	TRISA = 0x00;
	BANKSEL	_TRISA
	CLRF	_TRISA
_00106_DS_
;	.line	15; "blink.c"	PORTA = 1;
	MOVLW	0x01
	BANKSEL	_PORTA
	MOVWF	_PORTA
;	.line	17; "blink.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	18; "blink.c"	PORTA = 0;
	BANKSEL	_PORTA
	CLRF	_PORTA
;	.line	20; "blink.c"	delay(RETARDO);
	MOVLW	0x10
	MOVWF	STK00
	MOVLW	0x27
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
	GOTO	_00106_DS_
	RETURN	
; exit point of _main

;***
;  pBlock Stats: dbName = C
;***
;entry:  _delay	;Function start
; 2 exit points
;has an exit
;5 compiler assigned registers:
;   r0x1000
;   STK00
;   r0x1001
;   r0x1002
;   r0x1003
;; Starting pCode block
_delay	;Function start
; 2 exit points
;	.line	24; "blink.c"	void delay( uint16_t time){
	BANKSEL	r0x1000
	MOVWF	r0x1000
	MOVF	STK00,W
	MOVWF	r0x1001
;	.line	26; "blink.c"	for (i=0; i<time; i++);
	CLRF	r0x1002
	CLRF	r0x1003
_00117_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00168_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00168_DS_
	BTFSC	STATUS,0
	GOTO	_00111_DS_
;;genSkipc:3247: created from rifx:0x7fff91b9ae60
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00117_DS_
_00111_DS_
;	.line	27; "blink.c"	for (i=0; i<time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00120_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00169_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00169_DS_
	BTFSC	STATUS,0
	GOTO	_00112_DS_
;;genSkipc:3247: created from rifx:0x7fff91b9ae60
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00120_DS_
_00112_DS_
;	.line	28; "blink.c"	for (i=0; i<time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00123_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00170_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00170_DS_
	BTFSC	STATUS,0
	GOTO	_00113_DS_
;;genSkipc:3247: created from rifx:0x7fff91b9ae60
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00123_DS_
_00113_DS_
;	.line	29; "blink.c"	for (i=0; i<time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00126_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00171_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00171_DS_
	BTFSC	STATUS,0
	GOTO	_00114_DS_
;;genSkipc:3247: created from rifx:0x7fff91b9ae60
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00126_DS_
_00114_DS_
;	.line	30; "blink.c"	for (i=0; i<time; i++);
	BANKSEL	r0x1002
	CLRF	r0x1002
	CLRF	r0x1003
_00129_DS_
	BANKSEL	r0x1000
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00172_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00172_DS_
	BTFSC	STATUS,0
	GOTO	_00131_DS_
;;genSkipc:3247: created from rifx:0x7fff91b9ae60
	BANKSEL	r0x1002
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00129_DS_
_00131_DS_
	RETURN	
; exit point of _delay


;	code size estimation:
;	   88+   22 =   110 instructions (  264 byte)

	end
