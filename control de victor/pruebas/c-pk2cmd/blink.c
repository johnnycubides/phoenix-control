#include <pic16f628a.h>
#include <stdint.h>

static __code uint16_t __at (0x2007) configword1 = 0x2118;

#define RETARDO 10000

//#define LED PORTAbits.RA0

void delay (uint16_t time);

void main(void){
    TRISA = 0x00;
    while(1){
        PORTA = 1;
        // LED = 1;
        delay(RETARDO);
        PORTA = 0;
        // LED = 0;
        delay(RETARDO);
    }
}

void delay( uint16_t time){
    uint16_t i;
    for (i=0; i<time; i++);
    for (i=0; i<time; i++);
    for (i=0; i<time; i++);
    for (i=0; i<time; i++);
    for (i=0; i<time; i++);
}