;******************************************
;Titulo: Parpadeo de un LED
;archivo: eje1.asm
;autor: Johnny Cubides
;e-mail: jgcubidesc@unal.edu.co
;------------------------------------------
;Descripción: Se conectará un LED a uno de 
;los GPIO del microcontrolador donde el LED
;será encendido por un tiempo de 250 ms y 
;luego apagado de forma intermitente
;-----------------------------------------

;--- Especificación PIC a usar
        list p=16f628a
        __CONFIG b'10000100011000'
;---Registros específicos
STATUS  equ     0x03
RP1     equ     6       ;6 = b'0000110' = 0x06
RP0     equ     5       ;5 = b'0000101' = 0x05
TRISA   equ     0x85
TRISB   equ     0x86
PORTA   equ     0x05
PORTB   equ     0x06

;---Variables
RA_0    equ     0
D1      equ     0x20
D2      equ     0x21

;---Configuración de registros
        org     0x00        ;Vector de Reset
        bsf     STATUS,RP0  ;pone en 1 el RP0 de estatus, entra al bank1
        movlw   0x00        ;Carga a el registro w con b'00000000'
        movwf   TRISA       ;Carga el valor de w en el registro TRISA
        bcf     STATUS,RP0  ;Regresa al bank0

;---Comienzo del programa
INICIO  bsf     PORTA,RA_0  ;Pone en alto el pin 0 de PORTA
        call    DELAY
        bcf     PORTA,RA_0  ;Pone en bajo el pin 0 de PORTA
        call    DELAY
        goto    INICIO

;----------------------------
; Delay = 0.25 seconds
; clock frequiency = 4 MHz
; LINK: http://www.golovchenko.org/cgi-bin/delay

; Actual de,ay = 0.25 seconds = 250000 cycles
; Error = 0 %

DELAY   movlw   0x4E
        movwf   D1
        movlw   0xC4
        movwf   D2
DELAY_0 decfsz  D1,1
        goto    $+2
        decfsz  D2,1
        goto    DELAY_0
        goto    $+1
        nop
        return
;---------------------------

END
