/*-----------------------------------------------------
Author:  --<>
Date: 2015-09-24
Description:

-----------------------------------------------------*/
#define a 7
#define b 6
#define c 5
#define d 4
int i;
void setup() {
    // put your setup code here, to run once:
    pinMode(a,OUTPUT);
    pinMode(b,OUTPUT);
    pinMode(c,OUTPUT);
    pinMode(d,OUTPUT); 
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
}

void loop() {
    // put your main code here, to run repeatedly:
    
    for(i=0; i<4; i++)
    {
        delay(3);
        switch (i) 
        {
            case 0 :
                digitalWrite(a, HIGH);
                digitalWrite(b, LOW);
                digitalWrite(c, HIGH);
                digitalWrite(d, LOW); 
                break;
        
            case 1 :
                digitalWrite(a, LOW);
                digitalWrite(b, HIGH);
                digitalWrite(c, HIGH);
                digitalWrite(d, LOW); 
                break;
                
            case 2 :
                digitalWrite(a, LOW);
                digitalWrite(b, HIGH);
                digitalWrite(c, LOW);
                digitalWrite(d, HIGH);
                break;
            
            case 3 :
                digitalWrite(a, HIGH);
                digitalWrite(b, LOW);
                digitalWrite(c, LOW);
                digitalWrite(d, HIGH); 
                break;               
                
        }
    
    }
}
