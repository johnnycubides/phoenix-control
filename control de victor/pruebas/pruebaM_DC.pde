/*-----------------------------------------------------
Author:  --<>
Date: 2015-09-22
Description:

-----------------------------------------------------*/

const int control = 11 ;

void setup()
{    
     pinMode(control,  OUTPUT) ;
}

void loop()
    { 
         digitalWrite(control, HIGH);
         delay(2000);
         digitalWrite(control, LOW);
         delay(1000);
    }