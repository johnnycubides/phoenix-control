/*-----------------------------------------------------
Author:  --<>
Date: 2015-09-22
Description:

-----------------------------------------------------*/

const int control = 11 ;
const int poten = 13 ;

float vel ;


void setup()
{    
    pinMode(control, OUTPUT) ;
    pinMode(poten, INPUT) ;
}

void loop()
{ 
    vel = analogRead(poten);
    analogWrite(control, vel);
    delay(1000);
}
