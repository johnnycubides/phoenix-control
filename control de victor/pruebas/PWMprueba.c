/*La generación del PWM saldrá por el pin asignado como CCP1, en este caso se
trata del PIN RB3 pin9*/

#include <16f628a.h>
#fuses INTRC, NOWDT, NOPROTECT, BROWNOUT, PUT, NOLVP
#use delay(clock=4000000)

void main()
{
   int i = 0;

   output_low(PIN_B3); //CCP1
   setup_ccp1(CCP_PWM);

   setup_timer_2(T2_DIV_BY_16, 255, 1);


   while(true)
   {

      for(i=0; i < 255; i++)
      {
         set_pwm1_duty(i);
         delay_ms(5);
      }
      for(i = 255; i > 0; i--)
      {
         set_pwm1_duty(i);
         delay_ms(5);
      }
   }
}
