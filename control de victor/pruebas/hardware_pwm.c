#include <16f877.h>         // identifica microcontrolador alvo
#device ADC=10              // define AD para 10 bits, variando de 0 a 1023
#use delay (clock=4000000)  // <- define cristal para 4Mhz. Para outros valores, mude e recompile.
#include <cuscostdio.h>     // inclui biblioteca de fun��es do projeto CUSCOPiC

main()
 {
 long duty=0;
 short sobe = 1;
 setup_ccp1(CCP_PWM);  // ativa PWM1 (pino C2)
 setup_ccp2(CCP_PWM);  // ativa PWM2 (pino C1)
 
/*
Lembrando: PWM significa modula��o por largura de pulso. Precisamos configurar 2 coisas:
1) a freq��ncia de sa�da. Esta manteremos constante ap�s a configura��o. Iremos configurar como TEMPO DE CICLO
2) o tempo do pulso ativo em cada ciclo. � chamado de DUTY CICLE, que � a taxa vari�vel da sa�da.

Abaixo, configuramos o TEMPO DE CICLO.
� calculado pelas vari�veis TEMPO DE INSTRU��O (que � sempre  4/valor do clock).
                            DIVISOR (PODE SER T2_DIV_BY_1, T2_DIV_BY_4, T2_DIV_BY_16)
                            PERIODO (PODE SER DE 0 A 255)
                            POSTSCALE (PARA O PWM, DEIXE EM 1)

EXEMPLO: PARA CRISTAL DE 4Mhz
   setup_timer_2(T2_DIV_BY_4, 255, 1) --> TEMPO CICLO = (4/4000000) * 4 * 255 = 0,00102s = 1,02 ms (aprox. 1KHz)

         COM A MESMA LINHA E CRISTAL DE 20Mhz
                                      --> TEMPO CICLO = (4/20000000) * 4 * 255 = 0,000204s = 0,204 ms (aprox. 4KHz)
                                      
   setup_timer_2(T2_DIV_BY_16, 100, 1) p/ clock de 10 MHz:
                                      --> TEMPO CICLO = (4/10000000) * 16 * 100 = 0,00064s = 0,64 ms (aprox. 1,5KHz)
*/


 setup_timer_2(T2_DIV_BY_4, 255, 1); // define tempo de ciclo. Varia conforme CLOCK
 

 while(1)
   {
   lcd_gotoxy(1,1);
   if (!toggle(D0))
      {
      lcd_putc("MODO AUTOMATICO");
      if (sobe)
         {
         duty ++;
         }
      else
         {
         duty --;
         }
     if (duty == 1023) sobe = 0;
     if (duty == 0)    sobe = 1;
     }
  else
     {
     lcd_putc("MODO MANUAL    ");
     if (input(PIN_D6) && duty < 1023) duty ++;
     if (input(PIN_D7) && duty > 0)    duty --;
     }
   printf(lcd_putc,"\nTaxa: %04lu", duty);
   set_pwm1_duty(1023-duty);
   set_pwm2_duty(duty);
   delay_ms(5);
   }
 setup_ccp1(CCP_OFF);
 setup_ccp2(CCP_OFF);
 }
