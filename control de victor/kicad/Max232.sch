EESchema Schematic File Version 2  date mié 27 ago 2014 19:18:29 COT
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25  0
EELAYER END
$Descr User 6000 5000
encoding utf-8
Sheet 1 1
Title "MAX232 DB9"
Date "28 aug 2014"
Rev ""
Comp "Peronal"
Comment1 "Autor: Johnny German Cubides Castro"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 3950 2400
Wire Wire Line
	3950 2400 3950 2950
Wire Wire Line
	3950 2950 4300 2950
Connection ~ 4000 2350
Wire Wire Line
	4000 2350 4000 2850
Wire Wire Line
	4000 2850 4300 2850
Wire Wire Line
	3200 1900 3500 1900
Wire Wire Line
	4100 1450 4100 1550
Wire Wire Line
	4100 1550 3900 1550
Wire Wire Line
	3350 1300 3350 1350
Wire Wire Line
	3350 1300 3200 1300
Connection ~ 3900 1550
Wire Wire Line
	3750 850  3750 1600
Wire Wire Line
	3750 1600 3600 1600
Connection ~ 3850 1650
Wire Wire Line
	3850 1800 3850 1650
Connection ~ 4150 1250
Wire Wire Line
	4150 850  4150 1250
Wire Wire Line
	1600 2600 900  2600
Wire Wire Line
	900  2600 900  950 
Wire Wire Line
	900  950  2900 950 
Wire Wire Line
	2900 950  2900 1050
Wire Wire Line
	2900 1050 4400 1050
Wire Wire Line
	1600 1800 1350 1800
Wire Wire Line
	1600 1300 1350 1300
Wire Wire Line
	4300 2250 4150 2250
Wire Wire Line
	4150 2250 4150 2400
Wire Wire Line
	4150 2400 3200 2400
Wire Wire Line
	3500 1900 3500 1650
Wire Wire Line
	3500 1650 4300 1650
Wire Wire Line
	4400 1250 3900 1250
Wire Wire Line
	3900 1250 3900 1650
Connection ~ 3900 1650
Wire Wire Line
	3200 2600 3400 2600
Wire Wire Line
	3400 2600 3400 2350
Wire Wire Line
	3400 2350 4100 2350
Wire Wire Line
	4100 2350 4100 2050
Wire Wire Line
	4100 2050 4300 2050
Wire Wire Line
	1600 1700 1350 1700
Wire Wire Line
	1600 2200 1350 2200
Wire Wire Line
	4400 1150 2800 1150
Wire Wire Line
	2800 1150 2800 1050
Wire Wire Line
	2800 1050 1050 1050
Wire Wire Line
	1050 1050 1050 2400
Wire Wire Line
	1050 2400 1600 2400
Connection ~ 3750 1350
Wire Wire Line
	3850 2200 3200 2200
Wire Wire Line
	3200 1600 3200 1700
Wire Wire Line
	3450 650  3450 1350
Connection ~ 3450 1350
Wire Wire Line
	3350 1350 4400 1350
Wire Wire Line
	4300 2750 4050 2750
Wire Wire Line
	4050 2750 4050 1650
Connection ~ 4050 1650
$Comp
L CONN_3 K1
U 1 1 4FB39E81
P 4650 2850
F 0 "K1" V 4600 2850 50  0000 C CNN
F 1 "CONN_3" V 4700 2850 40  0000 C CNN
	1    4650 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 4FB3065D
P 4100 1450
F 0 "#PWR01" H 4100 1450 30  0001 C CNN
F 1 "GND" H 4100 1380 30  0001 C CNN
	1    4100 1450
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 4FB30624
P 3450 650
F 0 "#PWR02" H 3450 750 30  0001 C CNN
F 1 "VCC" H 3450 750 30  0000 C CNN
	1    3450 650 
	1    0    0    -1  
$EndComp
NoConn ~ 1600 2500
NoConn ~ 1600 2700
NoConn ~ 3200 2500
NoConn ~ 3200 2700
NoConn ~ 4300 2450
NoConn ~ 4300 2350
NoConn ~ 4300 2150
NoConn ~ 4300 1950
NoConn ~ 4300 1850
NoConn ~ 4300 1750
Text Notes 5000 1650 0    60   ~ 0
GND
Text Notes 4900 1100 0    60   ~ 0
RX\nTX\nGND\nVCC
Text Label 3100 3000 0    60   ~ 0
MAX232
$Comp
L CP1 CP4
U 1 1 4FB2DC7D
P 3400 1600
F 0 "CP4" H 3450 1700 50  0000 L CNN
F 1 "1µf" H 3450 1500 50  0000 L CNN
	1    3400 1600
	0    -1   -1   0   
$EndComp
$Comp
L CP1 CP1
U 1 1 4FB2DC2D
P 3850 2000
F 0 "CP1" H 3900 2100 50  0000 L CNN
F 1 "1µF" H 3900 1900 50  0000 L CNN
	1    3850 2000
	1    0    0    -1  
$EndComp
$Comp
L MAX232 U1
U 1 1 4FB2D0CA
P 2400 2000
F 0 "U1" H 2400 2850 70  0000 C CNN
F 1 "MAX232" H 2400 1150 70  0000 C CNN
	1    2400 2000
	1    0    0    -1  
$EndComp
$Comp
L CP1 CP2
U 1 1 4FB2D75D
P 1350 2000
F 0 "CP2" H 1400 2100 50  0000 L CNN
F 1 "1µf" H 1400 1900 50  0000 L CNN
	1    1350 2000
	1    0    0    -1  
$EndComp
$Comp
L CP1 CP3
U 1 1 4FB2D74D
P 1350 1500
F 0 "CP3" H 1400 1600 50  0000 L CNN
F 1 "1µf" H 1400 1400 50  0000 L CNN
	1    1350 1500
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P1
U 1 1 4FB2D5A5
P 4750 1200
F 0 "P1" V 4700 1200 50  0000 C CNN
F 1 "CONN_4" V 4800 1200 50  0000 C CNN
	1    4750 1200
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 4FB2D475
P 3950 850
F 0 "C1" H 4000 950 50  0000 L CNN
F 1 "1nf" H 4000 750 50  0000 L CNN
	1    3950 850 
	0    -1   -1   0   
$EndComp
$Comp
L DB9 J1
U 1 1 4FB2D201
P 4750 2050
F 0 "J1" H 4750 2600 70  0000 C CNN
F 1 "DB9" H 4750 1500 70  0000 C CNN
	1    4750 2050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
