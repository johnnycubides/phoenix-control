#include <16f628a.h>
#fuses NOWDT         //Deshabilita Watchdog
#fuses XT            //Cristal Externo
//#fuses RC                //Oscilador RC (Resistencia Capacitor Externos)
//#fuses HS                //Modo Full Power de alta Velocidad, hasta 10MHZ, más depende el micro
//#fuses LP                //Modo Bajo Consumo hasta 200KHZ
//#fuses INTRC         //Cristal Interno RC
#use delay(clock=4000000)
void main()
{
   set_tris_b(0);
   //setup_oscillator(OSC_4MHZ);       
      //No es necesaria esta instruccion debido a que al momento de usar INTRC y                    
      //delay(clock=...) el Oscilador interno se programa en automatico
   output_low(PIN_B1);
    
   while(true)
   {
      output_toggle(PIN_B1);           //Cambiamos el estado del Pin,
      delay_ms(250);                   //Esperamos a lo sonso 100 ms
   }
}
