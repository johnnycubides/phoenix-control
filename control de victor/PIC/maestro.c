#include <16f628a.h>
#fuses XT, NOWDT
#use delay (clock=4M)

#byte port_a = 0x05
#byte port_b = 0x06

#use I2C(MASTER, SDA=PIN_B4, FAST, SCL=PIN_B3)
/*xmit-> trasmisor, rcv= recepcion*/
#use rs232(baud=9600, xmit=PIN_B2, rcv=PIN_B1, bits=8, parity=N)

BYTE adress[1];
BYTE dato[1];
BYTE devices=1;


void RS232(BYTE device ){
	adress[device]=getc();
	dato[device]=getc();
}

void i2c_enviar(BYTE adress, BYTE dato){
	i2c_start();
	i2c_write(adress);
	i2c_write(dato);
	i2c_stop();
}

void main(){
	set_tris_b(0x00);
	output_high(PIN_B5);
	delay_ms(1000);
	output_low(PIN_B5);
	delay_ms(1000);
//	eneable_interrupts(int_rda);
	while(true){
		int i;
		for(i=0; i<devices; i++){
			output_toggle(PIN_B5);
			RS232(i);
		}
		for(i=0; i<devices; i++){
		i2c_enviar(adress[i], dato[i]);
		}
	}
}	
