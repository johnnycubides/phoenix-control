#include <16F876a.h> //Se incluye la librería del dispositivo
#fuses XT, NOWDT /*Aqui se configura tales como el perro guardian y, el tipo de reloj*/
#use delay (clock=4M) /*Configuración de la velocidad del reloj*/

#byte port_a =0x05
#byte port_b =0x06 
#byte entrada =0xff
#byte salida =0x00
#byte apagado =0x00

#use I2C(SLAVE, SDA=PIN_C4, FAST, SCL=PIN_C3, ADDRESS=0xa0, NOFORCE_SW)

void main(){
	set_tris_b(salida);
	unsigned long T1 =1500; ;
	int i;
	output_bit(pin_b2, 1);
	delay_ms(1000);
	output_bit(pin_b2, 0);
	delay_ms(1000); 
	output_bit(pin_b2, 1);
	while(true){
		T1=i2c_read();
		output_toggle(pin_b2);
		for(i=0; i<10;i++){
		OUTPUT_HIGH(PIN_B5);	
		delay_us(T1);
		OUTPUT_LOW(PIN_B5);
		delay_us(20000-T1);
		}
	}
}
