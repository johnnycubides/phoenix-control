#include <16f628a.h>
#fuses XT, NOWDT
#use delay (clock=4M)

#byte port_a = 0x05
#byte port_b = 0x06

#use I2C(MASTER, SDA=PIN_B4, FAST, SCL=PIN_B3)
/*xmit-> trasmisor, rcv= recepcion*/
#use rs232(baud=9600, xmit=PIN_B2, rcv=PIN_B1, bits=8, parity=N)

void main(){
	while(true){
		i2c_start();
		i2c_write(0xa0);
		i2c_write(100);
		i2c_stop();
		delay_ms(1000);
		i2c_start();
		i2c_write(0xa0);
		i2c_write(200);
		i2c_stop();
		delay_ms(1000);
		i2c_start();
		i2c_write(0xa0);
		i2c_write(20);
		i2c_stop();
		delay_ms(1000);
	}
}
