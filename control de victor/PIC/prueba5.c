#include <16f876a.h> //Se incluye la librería del dispositivo
#fuses XT, NOWDT /*Aqui se configura tales como el perro guardian y, el tipo de reloj*/
#use delay (clock=4M) /*Configuración de la velocidad del reloj*/

#byte port_a =0x05
#byte port_b =0x06 

#byte entrada =0xff
#byte salida =0x00
#byte apagado =0x00

#use I2C(SLAVE, SDA=PIN_C4, fast, SCL=PIN_C3, ADDRESS=0xa0)
/*xmit-> trasmisor, rcv= recepcion*/
//#use rs232(baud=9600, xmit=PIN_B2, rcv=PIN_B1, bits=8, parity=N)

long t1=1500, dataIn;

void main(){

	set_tris_b(salida);

	port_b=0;
	output_bit(PIN_B7, 1);
	delay_ms(1000);
	output_bit(PIN_B7, 0);
	delay_ms(1000); 
	output_bit(PIN_B7, 1);

	while(true){
		output_low(PIN_B6);	
		delay_us(t1);
		output_high(PIN_B6);
		delay_us(4000-t1);
		if(i2c_poll()){
			dataIn=i2c_read();
			t1=980+(dataIn*4);		//990 se supone pero de pende de un desfase de tiempo
			output_toggle(PIN_B5);
		}
	}
}
//CONCLUSIONES
//El dato recibido por el puierto I2C, al parecer se demora demaciado, tanto que hace variar la frecuecia
//haré una prueba más a una velocidad de entrada de datos
//Definitivamente NO es optimo, volveré a la prueba 4
//END 
