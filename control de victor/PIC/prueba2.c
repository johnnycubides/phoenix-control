#include <16f876a.h> //Se incluye la librería del dispositivo
#fuses XT, NOWDT /*Aqui se configura tales como el perro guardian y, el tipo de reloj*/
#use delay (clock=4M) /*Configuración de la velocidad del reloj*/

#byte port_a =0x05
#byte port_b =0x06 

#byte entrada =0xff
#byte salida =0x00
#byte apagado =0x00

#use I2C(SLAVE, SDA=PIN_C4, fast, SCL=PIN_C3, ADDRESS=0xa0)
/*xmit-> trasmisor, rcv= recepcion*/
//#use rs232(baud=9600, xmit=PIN_B2, rcv=PIN_B1, bits=8, parity=N)

void main(){
	set_tris_b(salida);
	int t1;
	//int i;
	port_b=0;
	output_bit(PIN_B7, 1);
	delay_ms(1000);
	output_bit(PIN_B7, 0);
	delay_ms(1000); 
	output_bit(PIN_B7, 1);
	while(true){
		if(i2c_poll()){
		t1=i2c_read();
		output_toggle(PIN_B7);
			if(t1==100){
				output_toggle(PIN_B5);
				output_low(PIN_B4);
			}
			else if(t1==200){
				output_toggle(PIN_B6);
				output_low(PIN_B4);
			}
			else{
				output_high(PIN_B4);
			}
		}
	}
	//	output_toggle(pin_b2);
	//	for(i=0; i<10;i++){
	//	OUTPUT_HIGH(PIN_B5);	
	//	delay_us(T1);
	//	OUTPUT_LOW(PIN_B5);
	//	delay_us(20000-T1);
	//printf("entero recibido %U", T1);
}
