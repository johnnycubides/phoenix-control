#!/usr/bin/env python

#importamos el modulo socket y serial
import socket
import serial
import string
#import time

UART = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)


def ordenPinguino (x):
    #print x
    #y = string.atoi(x)
    #print y
    UART.write("%c" % string.atoi(x))
    #print x

def main():
	#Variables
	host = "192.168.0.9"
	print UART.name 
	
	UART.write('a')
	esclavos = int(UART.readline(1))
	#print type(esclavo1), esclavo1
	#esclavo = int(esclavo1)
	print "Numero de esclavos", esclavos

	#instanciamos un objeto para trabajar con el socket
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	#Con el metodo bind le indicamos que puerto debe escuchar y de que servidor esperar conexiones
	#Es mejor dejarlo en blanco para recibir conexiones externas si es nuestro caso
	s.bind((host, 9999))

	#Aceptamos conexiones entrantes con el metodo listen, y ademas aplicamos como parametro
	#El numero de conexiones entrantes que vamos a aceptar
	s.listen(1)
	
	#Instanciamos un objeto sc (socket cliente) para recibir datos, al recibir datos este
	#devolvera tambien un objeto que representa una tupla con los datos de conexion: IP y puerto
	sc, addr = s.accept()

	recibido = str(sc.recv(1024))
	print recibido
	print "Enviando numero de esclavos"
	sc.send("%s" % esclavos)

	while True:
		i = 0
		d = []
		c = []	
		#Recibimos el mensaje, con el metodo recv recibimos datos y como parametro
   		#la cantidad de bytes para recibir
		print "Esperando Orden"
		recibido = str(sc.recv(8))
		if recibido == "send":
			print "Esperando Data "
			for i in range(esclavos):
				d.append(str(sc.recv(8)))
				c.append(str(sc.recv(8)))
			
			for i in range(esclavos):
				ordenPinguino(d[i])
				ordenPinguino(c[i])
		
		else:
			break
	
		print "Datos recibidos: "
		print d, c

	print ("Fin de comunicacion.")
	#Cerramos la instancia del socket cliente y servidor y UART
	UART.close()
	sc.close()
	s.close()

main()
