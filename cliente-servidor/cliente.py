#!/usr/bin/env python

#importamos el modulo para trabajar con sockets
import socket

def leer():
    #se lee el dato ingresado por teclado
    dato = raw_input()
    while int(dato)>255 or int(dato)<0:
        dato = raw_input("Error, Ingrese de nuevo el dato: ")
    return dato

def main():
    host = "192.168.2.1"

    # Creamos el socket y lo conectamos.
    s = socket.socket()
 
    #Asignamos la direccion del servidor y el puerto de conexion
    s.connect((host, 9999))

    # Ahora enviamos un mensaje de conexion al servidor
    s.send("Comunicacion establecida")

    esclavos = int(s.recv(1024))
    print "Numero de esclavos", esclavos

    while True:
        i = 0
        d = []
        c = []

        orden = raw_input("send, close: ")
        s.send(orden)
        if orden == "send":
            for i in range(esclavos):
                men1 = "Ingrese Direccion esclavo"+ str(i+1)+": "
                print men1
                di = leer()
                
                while di in d:
                    print "La Direccion esclavo ya existe."
                    di = leer()
                
                men2 = "Ingrese Comando esclavo"+ str(i+1)+": "
                print men2
                co = leer()
                    
                d.append(di)
                s.send(str(d[i]))
                c.append(co)             
                s.send(str(c[i]))
                
            print "Verificando estados"
            print d, c
    
        elif mensaje == "close" :
            break
    
    print "Fin de comunicacion"
    
    s.close()

main ()
