/*-----------------------------------------------------
Author:  --<>
Date: 2015-08-22
Description:

-----------------------------------------------------*/
u8 c;

void setup() {
    // put your setup code here, to run once:

    Serial.begin(9600);
    pinMode(7,OUTPUT);
    pinMode(6,OUTPUT);
    pinMode(5,OUTPUT);
}

void loop() {
    // put your main code here, to run repeatedly:
    //
     //c = CDC.getKey();
 if (Serial.available()){
    c = Serial.read();
    switch (c) {
       case 'a':
           toggle(7);
           break;
       case 'b':
           toggle(6);
           break;
       case 'c':
           toggle(5);
           break;
       default :
           break;
    }
  }
}
